" Do go get for gocode and don't forget to add go to path
" $ go get -u github.com/nsf/gocode

if !has('nvim')
  set nocompatible " Don't act like vi we're the new kid
endif

" Auto install plugins on start {{{
" fun stuff
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif
" }}}

" Vim Plug stuff {{{
call plug#begin('~/.config/nvim/plugged')

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

  Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }
  Plug 'machakann/vim-highlightedyank'
  Plug 'dracula/vim', { 'as': 'dracula' }
  Plug 'Shougo/neco-syntax'
  Plug 'airblade/vim-gitgutter'
  Plug 'Shougo/neco-vim'
  Plug 'Shougo/neosnippet-snippets'
  Plug 'Shougo/neosnippet.vim'
  Plug 'altercation/vim-colors-solarized'
  Plug 'easymotion/vim-easymotion'
  Plug 'fatih/vim-go', { 'for': 'go' }
  Plug 'haya14busa/dein-command.vim'
  Plug 'honza/vim-snippets'
  Plug 'itchyny/lightline.vim'
  Plug 'jiangmiao/auto-pairs'
  Plug 'jreybert/vimagit', { 'on': 'Magit' }
  Plug 'junegunn/vim-easy-align'
  Plug 'junegunn/vim-fnr'
  Plug 'junegunn/vim-pseudocl'
  Plug 'lervag/vimtex'
  Plug 'majutsushi/tagbar', {'on': 'TagBarToggle'}
  Plug 'mhinz/vim-startify'
  Plug 'ntpeters/vim-better-whitespace'
  Plug 'sheerun/vim-polyglot'
  Plug 't9md/vim-choosewin'
  Plug 'tpope/vim-commentary'
  Plug 'tpope/vim-sensible'
  Plug 'tpope/vim-surround'
  Plug 'tweekmonster/deoplete-clang2'
  Plug 'tweekmonster/startuptime.vim'
  Plug 'w0rp/ale'
  Plug 'zchee/deoplete-go', {'do': 'make'}
  Plug 'zchee/deoplete-jedi'

  " Plug 'zchee/deoplete-clang'
call plug#end()
" }}}

" generic settings {{{
syntax on
colorscheme dracula
set autoindent                             " Copy indent from line when inserting new line
set autoread                               " Read in file changes from disk W/O asking
set background     =dark
set backup                                 " If you write to an existing file (but do not append) a backup is made
set backupdir      =~/.config/nvim/backups " Sets backup dir
set cindent                                " See previous much more smarterer
set colorcolumn    =81
set cursorline                             " Highlight current line
set directory      =~/.config/nvim/swaps   " Sets swap dir
set encoding       =utf-8                  " Obvious
set expandtab                              " Insert tabs instead of spaces
set fileencodings  =utf-8                  " Obvious
set fileformats    =unix,mac,dos           " Prefer unix line endings but work with mac & dos
set foldmethod     =marker                 " Sets folds to show on marker instead of indent
set guioptions     =M                      " Disables loading of certain GUI menus to speed up load times
set hidden                                 " When switching buffers hides them instead of closing them
set hlsearch                               " Highlight search results
set ignorecase                             " Ignore case in search
set incsearch                              " Show matches as you type
set lazyredraw                             " Let vim do the minimal amount of redraws
set noerrorbells                           " Fuck them again to be safe
set number                                 " Line numbers
set relativenumber                         " Displays relative line distance from current line
set scrolloff      =3                      " Preserve 3 line above and below cursor at all times, unless EOF
set shiftround                             " I'm honestly not too sure, keep to be safe
set shiftwidth     =2                      " See tabstop
set showmatch                              " Highlight matching thing from under cursor
set smartcase                              " Ignorecase but smarter
set smartindent                            " See previous but smarter
set smartindent                            " Smart indenting on new line
set smarttab                               " Pretty much same as above
set softtabstop    =2                      " Number of space a tab counts for in operations
set splitbelow                             " Move cursor to bottom spit on open
set splitright                             " Move cursr to right split on open
set synmaxcol      =200                    " Keep vim from lagging stop doing syntax after x column
set tabstop        =2                      " Number of Spaces a tab counts as
set title                                  " Sets terminal title to title of file
set undolevels     =1000                   " Obvious
set virtualedit   +=block                  " Allowed moving the cursor anywhere in visual block mode
set visualbell                             " Fuck the error beep give me a visual
set wildmenu                               " Adds autocompletion for file path in command mode
set writebackup                            " Make a backup before overwriting a file.
" }}}

" plugin settings {{{
let g:deoplete#enable_at_startup        = 1
let g:EasyMotion_smartcase              = 1
let did_install_default_menus           = 1
let did_install_syntax_menu             = 1
let g:strip_whitespace_on_save          = 1
let g:choosewin_overlay_enable          = 1
let g:netrw_banner                      = 0
let g:netrw_liststyle                   = 3
let g:netrw_browse_split                = 4
let g:netrw_altv                        = 1
let g:netrw_winsize                     = 25
let g:ale_c_parse_makefile              = 1
let g:go_fmt_command                    = "goimports"
let g:vimtex_view_method                = 'zathura'
let g:deoplete#sources#go#gocode_binary = '/home/tigew/go/bin/gocode'
let g:go_fmt_command                    = "goimports"
" let g:deoplete#sources#clang#libclang_path = '/usr/lib/libclang.so'
" let g:deoplete#sources#clang#clang_header = '/lib/clang'

" SuperTab like snippets behavior.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
" Changed vim's complete() to be able to moved with tab
imap <expr><TAB>
\ pumvisible() ? "\<C-n>" :
\ neosnippet#expandable_or_jumpable() ?
\    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
    \ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
" }}}

" remaps - Commenting these seems to break thigns {{{
let mapleader = "\<Space>"
" nnoremap <Space>   <PageDown>
" xnoremap <Space>   <PageDown>
nnoremap <C-J>     <C-W><C-J>
nnoremap <C-K>     <C-W><C-K>
nnoremap <C-L>     <C-W><C-L>
nnoremap <C-H>     <C-W><C-H>
" map      <Leader>f <Plug>(easymotion-bd-f)
" nmap     <Leader>f <Plug>(easymotion-overwin-f)
nmap      <Leader>[ :Vexplore<CR>
nmap     <Leader>] :TagbarToggle<CR>
nmap     -         <Plug>(choosewin)
map      <Leader>c :Commentary<CR>
nmap     <Leader>x :let      @/=''<CR>
vmap     y         ygv<Esc>
vnoremap >         ><CR>gv
vnoremap <         <<CR>gv
nmap     <silent>  <Leader>s :split<CR>
nmap     <silent>  <Leader>v :vsplit<CR>
nmap     <Leader>t "=strftime("%c")<CR>Pgcc
xmap     ga        <Plug>(EasyAlign)
nmap     ga        <Plug>(EasyAlign)
nnoremap j         gj
nnoremap k         gk
inoremap jf        <esc>
imap     <C-k> <Plug>(neosnippet_expand_or_jump)
smap     <C-k> <Plug>(neosnippet_expand_or_jump)
xmap     <C-k> <Plug>(neosnippet_expand_target)
" }}}

" More complex settings/function {{{
" Persistent undo
if has('persistent_undo')
  " Just make sure you don't run 'sudo vim' right out of the gate and make
  " ~/.config/nvim/undos owned by root.root (should probably use sudoedit anyhow)
  " ~/.vim/undos if in vim, change below to match
  let undo_base = expand("~/.config/nvim/undos")
  if !isdirectory(undo_base)
    call mkdir(undo_base)
  endif
  let undodir = expand("~/.config/nvim/undos/$USER")
  if !isdirectory(undodir)
    call mkdir(undodir)
  endif
  set undodir=~/.config/nvim/undos/$USER/
  set undofile
endif

let swap_dir = expand("~/.config/nvim/swaps")
if !isdirectory(swap_dir)
  call mkdir(swap_dir)
endif

let backup_dir = expand("~/.config/nvim/backups")
if !isdirectory(backup_dir)
  call mkdir(backup_dir)
endif

" Diff function for checking unsaved changed if needed
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()

" }}}
